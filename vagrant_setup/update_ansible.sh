
#!/bin/bash
#
# Since: 04/10/2020
# Author: dempsey
# Description: Remove and update ansible with the latest version
# that is needed to run the docker module
#

remove_ansible() {
    sudo yum remove -y ansible
}

update_yum_packages() {
    sudo yum -y update
}


install_epel_repository() {
    sudo yum install -y epel-release
}

install_ansible() {
    sudo yum install -y ansible
}

remove_ansible
install_epel_repository
update_yum_packages
install_ansible