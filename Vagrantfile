# -*- mode: ruby -*-
# vi: set ft=ruby :

IMAGE_NAME="cdempsey1221/ndms-centos7-x86_64-virtualbox"
WORKER_NODES=2

Vagrant.configure("2") do |config|
	config.ssh.insert_key = false # disable key based SSH because it looks like it messes up cluster comms
	config.vm.define "master" do |master|
        master.vm.box = IMAGE_NAME
		master.vm.hostname = 'k3s-master'
		master.ssh.username = 'vagrant'
		master.ssh.password = 'vagrant'
		master.vm.network "private_network", ip: "192.168.60.60", virtualbox__intnet: true
		master.vm.network "forwarded_port", guest: 6443, host: 6443, id: "k3sClusterApiPort"
		master.vm.network "forwarded_port", guest: 8001, host: 8001, id: "k3sProxyPort"
		master.vm.network "forwarded_port", guest: 8080, host: 8080, id: "ingressAuxPort"
		master.vm.network "forwarded_port", guest: 443, host: 443, id: "ingressSslPort"
		master.vm.network "forwarded_port", guest: 80, host: 80, id: "ingressPrimaryPort"
		master.vm.network "forwarded_port", guest: 22, host: 2000, id: "sshPort"
		master.vm.synced_folder ".", "/vagrant", mount_options: ["dmode=777"]
		#forward_port_range(master, 30100, 30200) # used for k3s and docker services.  only exposing 100 the port forwarding is SUPER SLOW in windows.
		master.vm.provider "virtualbox" do |vm|
			vm.memory = "1024"
			vm.cpus = "1"
		end
		update_ansible(master)
		add_docker_group_to_user(master)
		disable_firewalld(master)
		install_k3s(master)
		enable_user_namespaces(master)
	end
	(1..WORKER_NODES).each do |i|
		config.ssh.insert_key = false # disable key based SSH because it looks like it messes up cluster comms
        config.vm.define "node-#{i}" do |node|
            node.vm.box = IMAGE_NAME
			node.vm.network "private_network", ip: "192.168.60.#{i + 60}", virtualbox__intnet: true
			node.vm.network "forwarded_port", guest: 22, host: "221#{i + 1}", id: "sshPort"
			node.ssh.username = 'vagrant'
			node.ssh.password = 'vagrant'
			node.vm.hostname = "k3s-node-#{i}"
			node.vm.provider "virtualbox" do |vm|
				vm.memory = "2048"
				vm.cpus = "4"
			end
			disable_firewalld(node)
			node.vm.provision "shell", inline: <<-SHELL
			echo 'running ansible install for dependancies'
			ansible-playbook /vagrant/k3s.yml --extra-vars 'k3s_node_role=worker'
			SHELL
			add_docker_group_to_user(node)
			enable_user_namespaces(node)
		end
	end
end

def forward_port_range(node, min_port, max_port) 
	for p in min_port..max_port
		node.vm.network "forwarded_port", guest: p, host: p, id: "portf-#{p}"
	end
end

def update_ansible(node)
	node.vm.provision "shell", inline: <<-SHELL
	exec /vagrant/vagrant_setup/update_ansible.sh
	SHELL
end

def enable_user_namespaces(node)
	node.vm.provision "shell", inline: <<-SHELL
	echo 10000 > /proc/sys/user/max_user_namespaces
	SHELL
end

def disable_firewalld(node)
	node.vm.provision "shell", inline: <<-SHELL
	systemctl disable firewalld --now
	echo 'firewalld is disabled'
	SHELL
end

def add_docker_group_to_user(node)
	node.vm.provision "shell", :args => [node.ssh.username], inline: <<-SHELL
	echo 'Adding docker group to current user: '$1
	sudo usermod -aG docker $1
	echo 'finished install for user: '$1
	SHELL
end

def install_k3s(node)
	node.vm.provision "shell", inline: <<-SHELL
	echo 'running ansible install for master node dependancies'
	ansible-playbook /vagrant/k3s.yml
	SHELL
end
