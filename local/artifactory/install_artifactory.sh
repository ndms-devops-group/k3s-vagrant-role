
#!/bin/bash
#
# Since: 05/08/2020
# Author: dempsey
# Description: Install a local artifactory to k8s cluster via Helm.
#

artifactory_namespace="artifactory"

add_jfrog_repo() {
    helm repo add jfrog https://charts.jfrog.io
}

install_chart() {
    # Disable the default postgresql
    helm upgrade --install artifactory --set postgresql.enabled=false --namespace $artifactory_namespace jfrog/artifactory --set ingress.enabled=true --set artifactory.service.type=NodePort --set nginx.enabled=false -f artifactory_values.yml
}

create_namespace() {
    kubectl create namespace $artifactory_namespace
}

create_namespace
add_jfrog_repo
install_chart