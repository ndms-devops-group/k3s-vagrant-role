# What is K3s?
K3s is a fully compliant Kubernetes distribution with the following enhancements:

- Packaged as a single binary.
- Lightweight storage backend based on sqlite3 as the default storage mechanism. etcd3, MySQL, Postgres also still available.
- Wrapped in simple launcher that handles a lot of the complexity of TLS and options.
- Secure by default with reasonable defaults for lightweight environments.
- Simple but powerful “batteries-included” features have been added, such as: a local storage provider, a service load balancer, a Helm controller, and the Traefik ingress controller.
- Operation of all Kubernetes control plane components is encapsulated in a single binary and process.  This allows K3s to automate and manage complex cluster operations like distributing certificates.
- External dependencies have been minimized (just a modern kernel and cgroup mounts needed). K3s packages required dependencies, including:
    * containerd
    * Flannel
    * CoreDNS
    * CNI
    * Host utilities (iptables, socat, etc)
    * Ingress controller (traefik)
    * Embedded service loadbalancer
    * Embedded network policy controller

---

## What modifications have been made to k3s for this role?

* This installation is specificly designed to use Vagrant with Virtualbox.
* The NGINX ingress controller is being used instead of the default provider (Traefik).
* Docker is the containerization instead of the default (containerd).
* There's specific configuration for the default Virtualbox ethernet adapter (enp0s8).
* This configuration consists of the following:
    * k3s master node with an the following IP:  192.168.60.60
    * Two (2) k3s work nodes with a generated IP in the same subnet as the master [192.168.60.x]
    * Ports 80, 443, 6443 are all forwarded to the host.
* The NGINX ingress controller has the host ingress functionality turned **on by default** so it is accessible to the host running VirtualBox.
* Various changes had to be made to CentOS 7 in order to get k3s to deploy successfully.

---

## How do I install this role?

On the ansible control machine (host, VM, or even in some circumstances a local playbook installing via localhost) the following must be run to install the role:
```shell
$ ansible-galaxy install git+https://frc-web5d.navair.navy.mil/bitbucket/scm/ansible/k3s-vagrant-role.git
```

This will install the role to your local ansible control server.

---

## How do I use this role in a playbook?

It's easy!  Install the role using the command in the above section to install the k3s-vagrant role from Bitbucket.  Once that has been completed, it can be reference in a playbook with a roles directive.  Here's an example:
```yaml
# example_k3s_playbook.yml
---
- hosts: localhost
  become: yes

  vars:
    k3s_node_role: master
    install_helm: true
    install_nginx_ingress: true
    ingress_namespace: ingress-nginx
    use_host_networking: true
    helm_command: /usr/local/bin/helm

  roles:
    - role: 'k3s-vagrant'
```

---

## What important vars are available and what do they do?

| var | description | acceptable values |
| --- | ----------- | ----------------- |
| k3s_node_role | sets the behavior of the node to master or worker for installation | [master,worker] |
| install_helm | installs the helm k8s package manager | [true,false] |
| install_nginx_ingress | installs the the NGINX ingress controller into the cluster | [true,false] |
| ingress_namespace | installs the the NGINX ingress controller into the cluster | [any - valid k8s syntax] |
| use_host_networking | uses the hosts networking to bind to port 80,443 on the actual k8s network interfaces  | [true,false] |

**The defaults for these values are set in the playbook example above.**

---

### **Made by the NDMS DevOps Team**
```json
{
    "devops": {
        "members": {
        "parker": {"name": "Parker Garrison", "position": "Lead DevOps Engineer" },
        "chris": {"name": "Chris Dempsey", "position": "DevOps Engineer" }
        }
    }
}